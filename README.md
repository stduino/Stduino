# StduinoIDE
![IDE开发界面](https://images.gitee.com/uploads/images/2020/0727/202005_e93be30c_1809898.png "ide.png")
#### 介绍
 **

### StduinoIDE资源包（请勿直接git下载源码，源码无法直接运行）
** 

#### 软件架构
 **

### 请勿直接下载本源文件运行，受编码格式限制个别文件未能完全放至本仓库，请使用下面链接进行下载软件
** 


#### 安装教程

1.  请勿直接下载本源文件运行，受编码格式限制个别文件未能完全放至本仓库，请使用下面链接进行下载软件
2.  通过下面软件下载解压后即可正常运行
3.   Windows版下载链接：  [IDE软件完整版下载链接](https://pan.bnu.edu.cn/l/AJTF8z)   密码：mauw
4.   Linux &Mac版正在开发中~~~

#### 使用说明

1.  软件正常启动后从工具栏点击工具按钮选择程序下载方式St_link Or 串口下载，同时注意开发板板型选择
2.  点击工具栏左上角编译按钮进行程序编译
3.  点击上传按钮进行程序上传
4.  原大部分直接基于寄存器进行开发的函数库，目前已基本完成STM32F103中等容量芯片的支持，现因学业繁忙，接下来几个月没时间进行继续开发，遂暂停一段时间对本函数库的维护更新，在此期间为不影响Stduino IDE正常使用，现决定自软件1.01版本起采用Arduino_Core_STM32框架进行开发，如有需要参考之前相关函数库可至library仓库获取相关资源。[基于寄存器开发的Library库](https://gitee.com/stduino/libraries)



### 功能概要

1. GDB调试
1. 封装库下载安装
1. 代码自动补全提示
1. 中英文模式
1. UTF-8\GBK编码格式
1. 代码快速格式化
1. SWD\串口程序下载&调试（已验证）
1. DFU程序下载方式（待验证）
1. USB HID程序下载方式（待验证）
1. maple DFU程序下载方式（待验证）
1. maple DFU2程序下载方式（待验证）
1. MassStorage程序下载方式（待验证）


当前已经支持的芯片&板型列表:

- Stduino UNO&Nano
- Nucleo 144 boards
- Nucleo 64 boards
- Nucleo 32 boards
- Discovery boards
- Eval boards
- STM32MP1 series coprocessor boards
- Generic STM32F0 boards
- Generic STM32F1 boards
- Generic STM32F3 boards
- Generic STM32F4 boards
- Generic STM32H7 boards
- Generic STM32L0 boards
- 3D printer boards
- LoRa boards
- Electronic Speed Controller boards
- Generic flight controllers
- Garatronics boards
- Midatronics boards



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### Stduino官网

 stduino.com

 **看到这里的各位大佬们，感觉这个项目不错的话，顺手点下左下角小星星呗，谢谢您的鼓励** 